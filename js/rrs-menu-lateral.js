document.addEventListener("DOMContentLoaded", function(event) { 
  	Object.defineProperty(window, 'RRSMenuLateral', { value: new (function RRSMenuLateralScope(){
		
		var toggleMenu = function(e){
			if(e && e instanceof Event){
				e.preventDefault();
			}

			var container = document.querySelector('.rrs-page-container');

			if(container){
				if(container.classList.contains('rrs-menu-opened')){
					container.classList.remove('rrs-menu-opened');	
					container.classList.add('rrs-menu-closed');
				}
				else{
					if(container.classList.contains('rrs-menu-closed')){
						container.classList.remove('rrs-menu-closed');	
					}
					container.classList.add('rrs-menu-opened');
				}
			}
		};
		
		var toggleMenuElements = document.querySelectorAll('.rrs-page-container .rrs-toggle-menu');
		
		for(var i = 0; i < toggleMenuElements.length; i++){
			toggleMenuElements[i].addEventListener('click', toggleMenu, false ); 
		}

		var contentOverlay = document.querySelector('.rrs-page-container .rrs-page-content .rrs-content-overlay');

		if(contentOverlay){
			contentOverlay.addEventListener('click', toggleMenu, false);
		}

		this.toggleMenu = toggleMenu;
	})(), writable: false });
});